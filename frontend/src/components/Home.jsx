import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { Grid, Link, Typography } from "@mui/material";
import { useHistory } from "react-router-dom";

function Home() {
  const history = useHistory();

  const gotoBasic = () => {
    history.push("/basic");
  };

  const gotoAdvanced = () => {
    history.push("/advanced");
  };

  return (
    <div>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        style={{ paddingTop: 20 }}
      >
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            "& > :not(style)": {
              m: 1,
              width: 600,
              height: 128,
            },
          }}
        >
          <Paper
            elevation={3}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#2b9b97",
              cursor: "pointer",
            }}
            // onClick={() => gotoBasic()}
          >
            <Typography variant="h" component="h2" style={{ color: "white" }}>
              Actors Information Retrieval <br />
              ලෝකයේ නලුවන්ගේ විස්තර
            </Typography>
          </Paper>
        </Box>
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        style={{ marginTop: 20 }}
      >
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            "& > :not(style)": {
              m: 1,
              width: 300,
              height: 128,
            },
          }}
        >
          <Paper
            elevation={3}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#898282",
              cursor: "pointer",
            }}
            onClick={() => gotoBasic()}
          >
            <Typography variant="h" component="h2" style={{ color: "white" }}>
              Basic Search <br />
              මූලික සෙවීම
            </Typography>
          </Paper>
          <Paper
            elevation={3}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#50466d",
              cursor: "pointer",
            }}
            onClick={() => gotoAdvanced()}
          >
            <Typography variant="h" component="h2" style={{ color: "white" }}>
              Advanced Search <br />
              උසස් සෙවීම
            </Typography>
          </Paper>
        </Box>
      </Grid>
    </div>
  );
}

export default Home;
