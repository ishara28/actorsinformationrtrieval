import * as React from "react";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import { blue, blueGrey, red } from "@mui/material/colors";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import Chip from "@mui/material/Chip";


const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));


export default function ActorDetail({
  name,
  birthday,
  birthplace,
  nationality,
  relegion,
  personalLife,
  careerLife,
  school,
}) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <React.Fragment>
      <Card sx={{ marginTop: 5 , marginLeft : 2 , marginRight : 2 }}>
        <CardHeader
          style={{ backgroundColor: "#5F817D", color: "white" }}
          avatar={
            <Avatar
              sx={{ bgcolor: blue[500] }}
              aria-label="recipe"
            >
              {/* {name[0]} */}
            </Avatar>
          }
          //   action={
          //     <IconButton aria-label="settings">
          //       <MoreVertIcon />
          //     </IconButton>
          //   }
          title={<Typography variant="h6">{name}</Typography>}
          //   subheader={birthday}
          subheader={<span style={{ color: "white" }}>{birthday}</span>}
        />
        {/* <CardMedia
            component="img"
            height="194"
            image="/static/images/cards/paella.jpg"
            alt="Paella dish"
          /> */}
        <CardContent style={{ backgroundColor: "#afbfbd" }}>
          <Typography variant="subtitle2" component="h6">
          <Chip label="ජාතිය" color="primary" style={{backgroundColor : "#047389" , fontWeight  : "bold"}} /> : {nationality}
          </Typography>
          <br />
          <Typography variant="subtitle2" component="h6">
          {/* උපන් ස්ථානය : {birthplace} */}
          <Chip label="උපන් ස්ථානය" color="primary" style={{backgroundColor : "#047389" , fontWeight  : "bold"}} /> : {birthplace}
          </Typography>
          <br />
          <Typography variant="subtitle2" component="h6">
            {/* ආගම : {relegion} */}
            <Chip label="ආගම" color="primary" style={{backgroundColor : "#047389" , fontWeight  : "bold"}} /> : {relegion}
          </Typography>
          <br />
          <Typography variant="subtitle2" component="h6">
            {/* අධ්‍යාපන ආයතනය : {school} */}
            <Chip label="අධ්‍යාපන ආයතනය" color="primary" style={{backgroundColor : "#047389" , fontWeight  : "bold"}} /> : {school}
          </Typography>
          <br />
          <Typography variant="subtitle2" component="h6"> 
            {/* පෞද්ගලික ජීවිතය */}
            <Chip label="පෞද්ගලික ජීවිතය" color="primary" style={{backgroundColor : "#047389" , fontWeight  : "bold"} } /> 
          </Typography>
          <br />
          <Typography variant="subtitle2" color="text.secondary">
            {personalLife}
          </Typography>
        </CardContent>
        <CardActions disableSpacing style={{ backgroundColor: "#d6d6d6" }}>
          {/* <IconButton aria-label="add to favorites">
              <FavoriteIcon />
            </IconButton>
            <IconButton aria-label="share">
              <ShareIcon />
            </IconButton> */}
          <ExpandMore
            expand={expanded}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
            style={{ backgroundColor: "#5F817D", color: "white" }}
          >
            <ExpandMoreIcon />
          </ExpandMore>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent style={{ backgroundColor: "#d6d6d6" }}>
            <Typography variant="subtitle2" component="h6">
              
              <Chip label="වෘත්තිිය ජීවිතය" color="primary" style={{backgroundColor : "#047389" , fontWeight  : "bold"} } />
            </Typography>
            <br />
            <Typography variant="subtitle2" color="text.secondary">
              {careerLife}
            </Typography>
          </CardContent>
        </Collapse>
      </Card>
    </React.Fragment>
  );
}
