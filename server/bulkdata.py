from elasticsearch import Elasticsearch, helpers
from elasticsearch_dsl import Index
import json, re
import codecs
import unicodedata
# import queries

client = Elasticsearch(HOST="http://localhost", PORT=9200)
INDEX = 'actors_ishara'

# Creating index if not manually created
# def createIndex():
#     index = Index(INDEX, using=client)
#     res = index.create()
#     print(res)

def read_all_songs():
    with open('corpus/data.json', 'r', encoding='utf-8-sig') as f:
        all_songs = json.loads(f.read().replace("}\n{", "},\n{"))
        # all_songs = json.loads(f.read())
        res_list = [i for n, i in enumerate(all_songs) if i not in all_songs[n + 1:]]
        return res_list

def genData(song_array):
    for song in song_array:
        # Fields-capturing
        # print(song)
        name = song.get("නම", None)
        personal_life = song.get("පෞද්ගලික ජීවිතය",None)
        career = song.get("වෘත්තිය", None)
        birthday = song.get("උපන් දිනය", None)
        birth_place = song.get("උපන් ස්ථානය", None)
        education = song.get("අධ්‍යාපනය", None)
        nationality = song.get("ජාතිය", None)
        religion = song.get("ආගම", None)

        yield {
            "_index": "actors_ishara",
            "_source": {
                "නම": name,
                "පෞද්ගලික ජීවිතය": personal_life,
                "වෘත්තිය": career,
                "උපන් දිනය": birthday,
                "උපන් ස්ථානය": birth_place,
                "අධ්‍යාපනය": education,
                "ජාතිය": nationality,
                "ආගම": religion
            },
        }

# createIndex()
all_songs = read_all_songs()
helpers.bulk(client,genData(all_songs))
