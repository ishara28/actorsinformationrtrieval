# Actors Information Retrieval System 

**This is an information retrieval system of world-famous actors in the Sinhala language.**

Main Technologies Used

* ElasticSearch
* Kibana
* Frontend - React
* Backend Server - Flask

## Big View

![Big view](Data/images/Archi.png)


## Project Setting Up
### ElasticSearch Setup
1. Install ElasticSearch from [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html) (This project uses version 7.15.1)
- Startup the elastic search as [here](https://www.elastic.co/guide/en/elasticsearch/reference/master/starting-elasticsearch.html) (Port 9200)
2. Install Kibana from [here](https://www.elastic.co/guide/en/kibana/current/install.html) 
- Startup the Kibana as [here](https://www.elastic.co/guide/en/kibana/current/start-stop.html) (Port 5601)
3. Install ICU analysis plugin from [here](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-icu.html)

### Create ElasticSearch index

1. Use the below queries to create the index. Use kibana to run the queries. It is easy using kibana dev tools. 
```
PUT /actors_ishara
{
  "settings": {
    "index": {
      "analysis": {
        "analyzer": {
          "sinhalaAnalyzer": {
            "type": "custom",
            "tokenizer": "icu_tokenizer",
            "filter": ["edgeNgram"],
            "char_filter": ["dotFilter"]
          }
        },
        "filter": {
          "edgeNgram": {
            "type": "edge_ngram",
            "min_gram": 2,
            "max_gram": 50,
            "side": "front"
          }
        },
        "char_filter": {
          "dotFilter": {
            "type": "mapping",
            "mappings": ". => \\u0020"
          }
        }
      }
    }
  }
}
```

```
PUT /actors_ishara/_mappings/
{
  "properties": {
    "නම": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "පෞද්ගලික ජීවිතය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "වෘත්තිය ජීවිතය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "උපන් දිනය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "උපන් ස්ථානය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "අධ්‍යාපනය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "ජාතිය": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    },
    "ආගම": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      },
      "analyzer": "sinhalaAnalyzer",
      "search_analyzer": "standard"
    }
  }
}
```
## Add Corpus
- To add corpus to the ElasticSearch, use Bulk.py python file and execute it. It uses [Bulk API](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html) in elastic search.

## React Frontend
1. Execute below commands to startup React frontend
``` 
cd frontend
npm install
npm start
```
The app will startup in port 3000

## Flask Server

1. Download and setup [SinLing](https://github.com/nlpcuom/Sinling). You have to append project path to your path environment variable

2. Execute below command to startup server
```
cd server
pip install -r requirements.txt
python App.py
```
**After all of the above steps, you will get the react interface as below in port 3000.**

![search by actor name example](Data/images/App.png)

**There are main 2 functions in the system**
- Basic Search (මූලික සෙවීම)
- Advanced Search (උසස් සෙවීම)


## Basic Search (මූලික සෙවීම)

**Below are some queries which supports by basic search**
- Search actor by name

![Search actor by name](Data/images/B1.png)

- Search actors according to birthday

![Search actors according to birthday](Data/images/B2.png)

![Search actors according to birthday](Data/images/B3.png)

- Search for actors who went to a particular school

![Search for actors who went to a particular school](Data/images/B4.png)

- Search for religion (ආගම)  of an actor

![Search for religion (ආගම)  of an actor](Data/images/B5.png)

- Search for School (අධ්‍යාපන ආයතනය) of an actor

![Search for School (අධ්‍යාපන ආයතනය) of an actor](Data/images/B6.png)

- Search for nationality(ජාතිය) of an actor

![Search for nationality(ජාතිය) of an actor](Data/images/B7.png)

- Search for actors born in a particular country

![Search for actors born in a particular country](Data/images/B8.png)

**Basic Search Queries Used here**

- දමිතා අබයරත්න
- 1997 වර්ශයේ උපන් නලුවන්
- ජනවාරි මස උපන්දිනය ඇති නලුවෝ
- රාහුල විද්‍යාලයයේ උගත් අය / රාහුල විද්‍යාලයයේ ඉගෙනගත් අය
- ටෙනිසන් කුරේගේ ආගම
- දමිතා අබයරත්නගේ පාසල
- ජේම්ස් ඩීන්ගේ ජාතිය
- ඉන්දියාවේ උපන් අය

## Advanced Search (උසස් සෙවීම)

**You can see Advanced search interface as below**

![Advanced search interface](Data/images/A1.png)

**You can search according to below fields**

- නම
- උපන්දිනය
- උපන් ස්ථානය
- ආගම
- ජාතිය
- අධ්‍යාපන ආයතනය

**Fields support Autocomplete**

**As an example, Search actors according to Religion (ආගම)**

![Advanced search example](Data/images/A2.png)

### Types of Qeries Used
- Phrase Query
- Bool Query  (must & should)
- Multi match Query
- Match phrase Query
- Aggregation
- Boosting Query
- Match phrase prefix




